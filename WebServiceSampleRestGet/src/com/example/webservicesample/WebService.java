package com.example.webservicesample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class WebService {
	String url="http://192.168.1.36/dr_book/";
	
	
	public WebService() {
		
	}
	
	public String checkLogin(String userName,String passWord){
		
		// Server Method name for authentication.
				String methodName = "login.php";
				
				// Combining the web URL,method and argument values.
				String completeUrl = url + methodName + "?user=" + userName
						+ "&pwd=" + passWord;
				// To store the JSON string result from the server.
				String result = "";
				// Invoke the get web service with combined URL and store the JSON
				// string result value.
				result = invokeGetWebservice(completeUrl);
				try {
					
					JSONArray jsArray=new JSONArray(result);
					// Construct the JSON object with the URL result string.
					JSONObject jsonResult = jsArray.getJSONObject(0);
					
					// Parse the JSON string and store the access token.
					result= jsonResult.getString("login");
				} catch (JSONException e) {
					
				
				}

				return result;
		
	}
	
	/**
	 * This method is used for to invoke the GET web service. This is common for
	 * every web service call
	 * 
	 * @param weburl
	 *            This is the web address to call the web service.
	 * @return result This is the JSON format result string from the web
	 *         services. Eg: {"webresult":true}
	 */
	String invokeGetWebservice(String webUrl) {
		String result = "";
		webUrl=webUrl.replace(" ","%20");
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(webUrl);
		HttpResponse response;
		try {
			response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream inputstream= entity.getContent();
				BufferedReader bufferedreader = new BufferedReader(
						new InputStreamReader(inputstream), 2 * 1024);
				StringBuilder stringbuilder = new StringBuilder();
				String currentline = null;
				try {
					while ((currentline = bufferedreader.readLine()) != null) {
						stringbuilder.append(currentline + "\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				result = stringbuilder.toString();
				Log.e("Result", result);
				inputstream.close();
				return result;
			}
		} catch (ClientProtocolException e1) {
			
			Log.e("ClientProtocolException", e1.toString());
			return result;
			
		} catch (IOException e1) {
			
			Log.e("IOException", e1.toString());
			return result;
			
		}
		return result;
	}


}
